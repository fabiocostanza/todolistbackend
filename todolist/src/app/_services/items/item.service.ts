import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


import { environment } from '../../../environments/environment';
import { Item } from '../../_model/Item';

@Injectable({ providedIn: 'root' })
export class ApiService {

    constructor(private http: HttpClient) {    }

    getItem(id: number): Observable<any> {
    //  return this.http.get(`${environment.backendUrl}/tasklist/${id}`);
      return this.http.get(`${environment.backendUrl}/tasklist/${id}`)
      .pipe(map((res: any) => res));
    }

    addItem(item: Object): Observable<Object> {
      return this.http.post(`${environment.backendUrl}/tasklist`, item);
    }

    updateItem(id: number, value: any): Observable<Object> {
      return this.http.put(`${environment.backendUrl}/tasklist/${id}`, value);
    }

    removeItem(id: number): Observable<any> {
      return this.http.post(`${environment.backendUrl}/tasklist/${id}/delete`, { responseType: 'text' });
    }

    getItems(): Observable<any> {
      return this.http.get(`${environment.backendUrl}/tasklist`);
    }

}
