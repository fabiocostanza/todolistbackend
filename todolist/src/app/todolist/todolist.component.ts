import { Component, OnInit } from '@angular/core';
import { Item } from '../_model/Item';
import { ApiService } from '../_services/items/item.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {

	items : Item[];
	static instance: TodolistComponent;

	constructor(private service: ApiService, private router: Router) { /*private service: HttpItemService*/

		this.updateLocalItems();
	}

	updateLocalItems(){
		console.log("Updating items");
		//this.service.getItems().then(items => this.items = items);
    this.service.getItems()
      .subscribe((res: any) => {
          this.items = res;
          console.log(this.items);
        }, (err: any) => {
          console.log(err);
        }
      );
	}

  	ngOnInit() {}

  onRemove(id: number){
		//this.service.removeItem(item).then(() => this.updateLocalItems());
    this.service.removeItem(id)
      .subscribe((res: any) => {
          this.updateLocalItems()
        }, (err: any) => {
          console.log(err);
        }
      );
	}

	onEdit(id: number){
    //console.log(items.id);
		//this.service.getItem(items.id);
		this.router.navigate(['edit-item', id]);
	}
}
