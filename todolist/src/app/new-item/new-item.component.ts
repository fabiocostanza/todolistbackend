import { Component, OnInit } from '@angular/core';
import { Item } from '../_model/Item';
import { ApiService } from '../_services/items/item.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';




@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  	newItemForm : FormGroup;

    name = '';
    description = '';
    prod_price: number = null;



	constructor(private service: ApiService, private router: Router, private builder: FormBuilder) { /*private service: HttpItemService*/

		this.newItemForm = builder.group(new Item("", ""));
	}

  /*ngOnInit() {
    this.newItemForm = this.builder.group({
      'name' : [null, Validators.required],
      'description' : [null, Validators.required]
    });
  }
  */

	ngOnInit() {
	}

/*
	onSubmit(): void {
		this.addItem(this.newItemForm.value);
	}
  */

  onSubmit() {
    this.service.addItem(this.newItemForm.value)
      .subscribe((res: any) => {
          const id = res._id;
          this.router.navigateByUrl('/items');
        }, (err: any) => {
          console.log(err);
        }
      );
	}

}
