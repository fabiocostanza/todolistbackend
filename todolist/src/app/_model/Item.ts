export class Item {

	id: Number;
	name: String;
	description: String;

	constructor(name: String, description: String, id?: Number){

		this.name = name;
		this.description = description;
		this.id = id;
	}
}
