import { Component, OnInit } from '@angular/core';
import { Item } from '../_model/Item';
import { Observable } from "rxjs";
import { ApiService } from '../_services/items/item.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})


export class EditItemComponent implements OnInit {
  	editItemForm : FormGroup;
    _id: number = null;
    name = '';
    description = '';

  constructor(private service: ApiService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.getItem(this.route.snapshot.params['id']);
    this.editItemForm = this.formBuilder.group({
      'id' : [null, Validators.required],
      'name' : [null, Validators.required],
      'description' : [null, Validators.required]
    });
  }

  getItem(id: number) {
    this.service.getItem(id).subscribe((data: any) => {
      this._id = data._id;
      this.editItemForm.setValue({
        id: data.id,
        name: data.name,
        description: data.description
      });
    });
  }


  onSubmit() {
    this.service.updateItem(this.route.snapshot.params['id'], this.editItemForm.value)
      .subscribe((res: any) => {
          const id = res._id;
          this.router.navigateByUrl('/items');
        }, (err: any) => {
          console.log(err);
        }
      );
	}

}
