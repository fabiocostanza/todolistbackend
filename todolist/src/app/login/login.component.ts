import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';


import { AuthService } from '../_services/auth/auth.service';
import { environment } from '../../environments/environment';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading: Boolean = false;
  submitted: Boolean = false;
  returnUrl: string;
  error = '';

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private auth: AuthService, private router: Router) {
       // redirect to home if already logged in
       if (this.auth.currentUserValue) {
           this.router.navigate(['/']);
       }
   }

  ngOnInit() {
        this.loginForm = this.fb.group({
          email: ['admin@admin.com', Validators.required],
          password: ['admin', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/items';
  }

  get field() { return this.loginForm.controls; }

  onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.auth.login(this.field.email.value, this.field.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                //    this.router.navigateByUrl(environment.backendUrl + '/items');
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
  }
}
