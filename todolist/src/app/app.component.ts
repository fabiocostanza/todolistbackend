import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './_services/auth/auth.service';
import { User } from './_model/User';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentUser: User;
  title = 'todolist';

  constructor(
        private router: Router,
        private auth: AuthService
    ) {
        this.auth.currentUser.subscribe(x => this.currentUser = x);
    }

  logout() {
        this.auth.logout();
        this.router.navigate(['/login']);
  }


}
