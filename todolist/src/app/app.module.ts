import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TodolistComponent } from './todolist/todolist.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
//import { HttpClientModule }    from '@angular/common/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { JwtInterceptor } from './_guards/jwt.interceptor';
import { ErrorInterceptor } from './_guards/error.interceptor';

import { NewItemComponent } from './new-item/new-item.component';
import { LoginComponent } from './login/login.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { AuthService } from './_services/auth/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    TodolistComponent,
    NewItemComponent,
    LoginComponent,
    EditItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
  //providers: [ ApiService, AuthService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
