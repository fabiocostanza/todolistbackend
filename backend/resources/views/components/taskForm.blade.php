
{{ Form::label('name', 'Titolo Attività', ['class' => 'control-label']) }}
{{ Form::text('name', null, ['class' => 'form-control form-control-lg', 'placeholder' => 'Titolo Attività']) }}

{{ Form::label('description', 'Descrizione Attività', ['class' => 'control-label mt-3']) }}
{{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Descrizione Attività']) }}

{{ Form::label('due_date', 'Scadenza', ['class' => 'control-label mt-3']) }}
{{ Form::date('due_date', null, ['class' => 'form-control'])}}

<div class="row justify-content-center mt-3">
	<div class="col-sm-4">
		<a href="{{ route('task.index')}}" class="btn btn-block btn-secondary">Indietro</a>
	</div>
	<div class="col-sm-4">
		<button class="btn btn-block btn-primary">Salva attività</button>
	</div>
</div>
