@extends('layouts.main')

@section('title', 'Elenco attività')

@section('content')

<div class="row justify-content-center mb-3">
	<div class="col-sm-4">
		<a href="{{route ('task.create')}}" class="btn btn-block btn-success">Aggiungi Attività</a>
	</div>
</div>
@if($tasks->count() === 0)
	<p class="lead text-center">Nessuna attività presente nel database!</p>
	@else

		@foreach($tasks as $task)
		<div class="row">
			<div class="col-sm-12">
				<h3>{{$task->name}}
				<small>{{ $task->created_at}}</small></h3>
				<p>{{ $task->description}}</p>
				<h6>{{ \Carbon\Carbon::parse($task->due_date)->format('d/m/Y')}}</h6>
				{!!Form::open(['route' => ['task.destroy',$task->id],'method' => 'DELETE'])!!}
				<a href="{{route ('task.edit', $task->id)}}" class="btn btn-sm btn-primary">Modifica</a>
				<button type="submit" class="btn btn-sm btn-danger">Elimina</button>
				{!! Form::close() !!}
			</div>
		</div>
		<hr>

	@endforeach
@endif


<div class="row justify-content-center ">
	<div class="col-sm-6  text-center">
		{{ $tasks->links() }}
	</div>
</div>

@endsection
