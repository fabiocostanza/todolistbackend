<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/*
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('recover', 'AuthController@recover');

Route::get('/tasklist', 'ApiTodolist@getTodolist');
Route::post('/tasklist', 'ApiTodolist@getTodolist');
Route::put('/tasklist/{id}', 'ApiTodolist@putTodolist');
Route::delete('/tasklist/{id}', 'ApiTodolist@delTodolist');
*/


    Route::post('login', 'AuthController@login')->name('api.login');
    Route::middleware('jwt.auth')->post('logout', 'AuthController@logout')->name('api.logout');
    Route::middleware('auth')->post('refresh', 'AuthController@refresh')->name('api.refresh');
    Route::middleware('jwt.auth')->post('me', 'AuthController@me')->name('api.me');

    //TodoList
    Route::get('/tasklist', 'ApiTodolist@index');
    Route::post('/tasklist', 'ApiTodolist@create');
    Route::get('/tasklist/{id}', 'ApiTodolist@show');
    Route::put('/tasklist/{id}', 'ApiTodolist@update');
    Route::post('/tasklist/{id}/delete', 'ApiTodolist@destroy');









/*

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');
    Route::get('test', function(){
        return response()->json(['foo'=>'bar']);
    });
});
*/



/*

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::middleware('auth:api', 'cors')->group(function(){
	Route::post('details', 'AuthController@details');
    Route::post('logout', 'AuthController@logout');
    Route::post('update', 'AuthController@update');
});
*/
