<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Tymon\JWTAuth\Contracts\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
//use Tymon\JWTAuth\JWTAuth;
use JWTAuth;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;


class AuthController extends Controller
{

    public function login() {
        $credentials = request(['email', 'password']);
        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return response()->json([
            'token' => $token,
            'expires' => auth('api')->factory()->getTTL() * 60,
        ]);
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */

     public function logout(Request $request) {
         $token = $request->header('Authorization');
         // Invalidate the token



         try {
          //  JWTAuth::invalidate($token);
          auth()->logout();

           JWTAuth::parseToken()->invalidate( $token );
            //  JWTAuth::invalidate($token);
            //  JWTAuth::setToken($token)->invalidate();
          //  JWTAuth::invalidate(JWTAuth::getToken());
         //   JWTAuth::setToken($token);
         //   JWTAuth::invalidate(JWTAuth::getToken());
         //   JWTAuth::setToken($token)->invalidate();
         //   JWTAuth::invalidate($request->header('Authorization'));
             return response()->json(['success' => true, 'message'=> "Logout eseguito con successo."]);

         } catch ( TokenExpiredException $e ) {
               return response()->json(['success' => false, 'error' => 'Token scaduto, riprova.' ], 401);

         } catch (JWTException $e) {
             // something went wrong whilst attempting to encode the token
             return response()->json(['success' => false, 'error' => 'Logout non riuscito, riprova.'], 500);
         }
     }

     /**
     * Refresh Token
     * Invalidate the token, so user cannot use it anymore
     * and return a new token
     *
     * @param Request $request
     */
    public function refreshToken() {
        try {
            if (!($token = JWTAuth::getToken())) {
                return response()->json(['error' => 'Token not provided'], 400);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        /*
        $token = JWTAuth::getToken();
        if(!$token){
        	//throw new BadRequestHtttpException('Token not provided');
                    return response()->json(['error' => 'Token not provided'], 400);
        }
        */
        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e) {
            //throw new AccessDeniedHttpException('The token is invalid');
            return response()->json(['error' => 'The token is invalid'], 400);
        }
        return response()->json(compact('token'));
    }

    /**
     * verify token using the jwt.auth middleware
     * @return Response
     */
    public function AuthenticatedUser()
    {
        //$user = JWTAuth::parseToken()->authenticate();
        //return response()->json(compact('user'));
        //return response()->json($user);
        //return response()->json($request->all(););
        try {
            if (!($user = JWTAuth::parseToken()->authenticate())) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }


}
