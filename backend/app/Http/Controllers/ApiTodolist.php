<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Task;
#use Request;

class ApiTodolist extends Controller
{
    public function index()
    {
      $todos = Task::all();
      return response()->json($todos);
    }

    public function create(Request $request)
    {
      //Valido la richiesta
      $userData = $request->validate([
        'name' => 'required|string|max:255|min:3',
        'description' => 'required|string|max:10000|min:10',
      ]);

      $newTodo = new Task;
      $newTodo->name = $request->name;
      $newTodo->description = $request->description;
      $newTodo->due_date = date("Y-m-d");
    //  $newTodo->fill($userData);
    //  $newTodo->fill($request->all());
      $newTodo->save();
      return response()->json($newTodo);
    }

    public function show($id)
    {
      $todo = Task::find($id);
      if (empty($todo)) {
         return response()->json(['error' => 'Task id non valido']);
       }
      return response()->json($todo);
    }

    public function update(Request $request, $id)
    {
       $userData = $request->all();

       $todoToUpdate = Task::find($id);

       if(empty($todoToUpdate)) {
           return response()->json(['error' => 'Task-ID non trovato, aggiornamento non riuscito']);
        }

      $todoToUpdate->update($userData);
      return response()->json($todoToUpdate);
    }

    public function destroy($id)
    {
      $todo = Task::find($id);

       if (empty($todo)) {
         return response()->json(['error' => 'Task id non valido']);
       }

       $todo->delete();
       return response()->json([]);
    }

}
